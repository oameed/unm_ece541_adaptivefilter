# Adaptive Filtering Applied to Noise Cancellation

[[ECE541 Final Project]](doc/Report.pdf) 2020. Noakoasteen. _Adaptive Filtering Applied to Noise Cancellation_

## References

[1]. 1996. Hayes. _Statistical Digital Signal Processing and Modeling_  
[[2].](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-011-introduction-to-communication-control-and-signal-processing-spring-2010/) 2011. Oppenheim. Verghese. _Introduction to Communication, Control, and Signal Processing_, MIT OpenCourseWare  
[[3].](https://www.wavsource.com/) 0000. _WaveSource_  

_Useful_

[[4].](https://people.stat.sc.edu/hitchcock/stat520.html) 2021. Hitchcock. _STAT 520: Forecasting and Time Series_  

## Code Statistics
<pre>
github.com/AlDanial/cloc v 1.74  T=0.01 s (1598.1 files/s, 54480.9 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
MATLAB                           9             35             81            168
Markdown                         1             26              0             57
Bourne Shell                     1              4              0              4
-------------------------------------------------------------------------------
SUM:                            11             65             81            229
-------------------------------------------------------------------------------
</pre>
## How to Run
* `cd` to project main directory
* `./run/sh/adaptivefilter.sh`

## Experiments

### Adaptive Filtering of Stationary Signals

|     |     |     |
|:---:|:---:|:---:|
|**Wiener Filter**|**NLMS Algorithm**|**RLS Algorithm**|
|![][fig1]        |![][fig2]         |![][fig3]        |

[fig1]:run/adaptivefilter/WienerFilter.png
[fig2]:run/adaptivefilter/stationaryNLMS.png
[fig3]:run/adaptivefilter/stationaryRLS.png

### Adaptive Filtering of Non-Stationary Signals

|     |     |
|:---:|:---:|
|**NLMS Algorithm**|**RLS Algorithm**|
|![][fig4]         |![][fig5]        |

[fig4]:run/adaptivefilter/nonstationaryNLMS.png
[fig5]:run/adaptivefilter/nonstationaryRLS.png

### Recovering Corrupted Audio Signal using NLMS Adaptive Filtering

|     |     |
|:---:|:---:|
|**Original Audio**|                 |
|![][fig6]         |![][aud1]        |

[fig6]:run/adaptivefilter/sorry_dave.png
[aud1]:run/adaptivefilter/sorry_dave.wav 

|     |     |
|:---:|:---:|
|**Corrupted Audio**|                 |
|![][fig7]          |![][aud2]        |

[fig7]:run/adaptivefilter/sorry_dave_corrupted.png
[aud2]:run/adaptivefilter/sorry_dave_corrupted.wav

|     |     |
|:---:|:---:|
|**Recovered Audio**|                 |
|![][fig8]          |![][aud3]        |

[fig8]:run/adaptivefilter/sorry_dave_recovered.png
[aud3]:run/adaptivefilter/sorry_dave_recovered.wav








