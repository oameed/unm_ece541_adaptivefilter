%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                            %%%
%%% ECE 541 PROBABILITY THEORY AND STOCHASTIC PROCESSES %%%
%%% FINAL PROJECT FALL 2020                             %%%
%%% ADAPTIVE FILTERS APPLIED TO NOISE CANCELLING        %%%
%%% PLOTTER FUNCTION                                    %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plotter(D,X,V2,Y,LENGTH,NAME)
filename=fullfile(pwd,strcat(NAME,'.png'))       ;

figure 
subplot(3,1,1)
hold
plot(X ,'b'  ,'linewidth',1.5)
plot(D ,'r--','linewidth',1.5)
axis([0 LENGTH -1.5*max(abs(X )) 1.5*max(abs(X ))])
subplot(3,1,2)
plot(V2,'b'  ,'linewidth',1.5)
axis([0 LENGTH -1.5*max(abs(V2)) 1.5*max(abs(V2))])
subplot(3,1,3)
hold
plot(Y ,'b'  ,'linewidth',1.5)
plot(D ,'r--','linewidth',1.5)
axis([0 LENGTH -2                2               ])

saveas(gcf,filename,'png')
close all
end

