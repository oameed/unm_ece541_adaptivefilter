%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                            %%%
%%% ECE 541 PROBABILITY THEORY AND STOCHASTIC PROCESSES %%%
%%% FINAL PROJECT FALL 2020                             %%%
%%% ADAPTIVE FILTERS APPLIED TO NOISE CANCELLING        %%%
%%% AUDIO PLOTTER FUNCTION                              %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function plotter_audio(X,FS,INFO,NAME)
filename=fullfile(pwd,strcat(NAME,'.png'))       ;
figure 
position=get(gcf,'Position'          );
set         (gcf,'Position'     ,...
                 [position(1)   ,...
                  position(2)   ,...
                  position(3)   ,...
                  position(3)/5     ])
time = 0:seconds(1/FS):seconds(INFO.Duration);
time = time(1:end-1)                         ;
plot(time,X)
xlabel('Time')
ylabel('Audio Signal')
saveas(gcf,filename,'png')
close all
end

