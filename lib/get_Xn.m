%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                            %%%
%%% ECE 541 PROBABILITY THEORY AND STOCHASTIC PROCESSES %%%
%%% FINAL PROJECT FALL 2020                             %%%
%%% ADAPTIVE FILTERS APPLIED TO NOISE CANCELLING        %%%
%%% GET INSTANCE OF INPUT VECTOR Xn                     %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function Xn=get_Xn(X,P,I)
if I< P+1
    END=1              ;
else
    END=I-P            ;
end
index=I:-1:END         ;
Xn   =X(index)         ;
if size(Xn,1)<P+1
    D =(P+1)-size(Xn,1);
    Xn=[Xn; zeros(D,1)];
end
end

