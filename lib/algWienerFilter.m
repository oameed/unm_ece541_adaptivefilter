%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                                   %%%
%%% ECE 541 PROBABILITY THEORY AND STOCHASTIC PROCESSES        %%%
%%% FINAL PROJECT FALL 2020                                    %%%
%%% ADAPTIVE FILTERS APPLIED TO NOISE CANCELLING               %%%
%%% CALCULATE WIENER FILTER WEIGHTS AND GENERATE FILTER OUTPUT %%%
%%% by: OAMEED NOAKOASTEEN                                     %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function e=algWienerFilter(X,V,P)
    function W=get_wiener_weights(X,V,P)
        Rvv         =zeros(P+1  )                  ;
        Rxv         =zeros(P+1,1)                  ;
        [rvv,lagsvv]=xcorr(V,  'biased')           ;
        [rxv,lagsxv]=xcorr(X,V,'biased')           ;
        index_rvv_0 =find (lagsvv==0   )           ;
        index_rxv_0 =find (lagsxv==0   )           ;
        for i=0:P
            for j=0:P
                Rvv(i+1,j+1)=rvv((index_rvv_0+i)-j);
            end
            Rxv(i+1)        =rxv( index_rxv_0+i   );
        end
        W           =Rvv\Rxv                       ;
    end
e=zeros(size(X,1),1)                               ;
w=get_wiener_weights(X,V,P)                        ;
for k=1:size(X,1)
    Xn  =get_Xn(V,P,k)                             ;
    e(k)=X(k)-w'*Xn                                ;
end
end

