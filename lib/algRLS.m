%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                            %%%
%%% ECE 541 PROBABILITY THEORY AND STOCHASTIC PROCESSES %%%
%%% FINAL PROJECT FALL 2020                             %%%
%%% ADAPTIVE FILTERS APPLIED TO NOISE CANCELLING        %%%
%%% RECURSIVE LEAST SQUARES ALGORITHM                   %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function e=algRLS(Y,X,LAMBDA,P)
eps=1e-4                            ;
w  =zeros(P+1,1  )                  ;
PM =eps.*eye(P+1)                   ;
e  =zeros(size(X))                  ;
for i=1:size(Y,1)
    Xn   =get_Xn(X,P,i)             ;
    z    =PM*conj(Xn)               ;
    g    =(1/(LAMBDA+Xn'*z))*z      ;
    alpha=Y(i)-w'*Xn                ;
    w    =w+alpha*g                 ;
    e(i) =Y(i)-w'*Xn                ;
    PM   =(1/LAMBDA)*(PM-g*conj(z)');
end
end

