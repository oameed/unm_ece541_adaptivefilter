%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                            %%%
%%% ECE 541 PROBABILITY THEORY AND STOCHASTIC PROCESSES %%%
%%% FINAL PROJECT FALL 2020                             %%%
%%% ADAPTIVE FILTERS APPLIED TO NOISE CANCELLING        %%%
%%% GENERATE CORRUPTED AUDIO SIGNAL                     %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [y,Fs,info,x,v2]=get_corrupted_signal_audio(NAME)

    function V=get_AR1_noise(LENGTH,C,G)
        V     =zeros(LENGTH,1)                     ;
        for i=1:LENGTH
            if i==1
                V(i)=G(i)                          ;
            else
                V(i)=G(i)+C*V(i-1)                 ;
            end
        end
    end

filename=strcat(NAME,'.wav')                       ;
info    =audioinfo(filename)                       ;
[y,Fs]  =audioread(filename)                       ;
LENGTH  =size(y,1)                                 ;
g       =randn(LENGTH,1).*linspace(0.5,2.5,LENGTH)';
v1      =get_AR1_noise(LENGTH, 0.8,g)              ;
v2      =get_AR1_noise(LENGTH,-0.6,g)              ;
x       =y+v1                                      ;
end

