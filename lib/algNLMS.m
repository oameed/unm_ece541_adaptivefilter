%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                            %%%
%%% ECE 541 PROBABILITY THEORY AND STOCHASTIC PROCESSES %%%
%%% FINAL PROJECT FALL 2020                             %%%
%%% ADAPTIVE FILTERS APPLIED TO NOISE CANCELLING        %%%
%%% NORMALIZED LEAST MEAN SQUARES ALGORITHM             %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function e=algNLMS(Y,X,BETA,P)
eps=1e-4                        ;
w  =zeros(P+1,1  )              ;
e  =zeros(size(X))              ;
for i=1:size(Y,1)
    Xn  =get_Xn(X,P,i)          ;
    e(i)=Y(i)-w'*Xn             ;
    mu  =(BETA/(eps+norm(Xn)^2));
    w   =w+mu*e(i)*conj(Xn)     ;
end
end

