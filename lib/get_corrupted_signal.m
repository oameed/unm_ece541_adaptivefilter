%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                            %%%
%%% ECE 541 PROBABILITY THEORY AND STOCHASTIC PROCESSES %%%
%%% FINAL PROJECT FALL 2020                             %%%
%%% ADAPTIVE FILTERS APPLIED TO NOISE CANCELLING        %%%
%%% GENERATE CORRUPTED SIGNAL                           %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% TYPE  : 'stationary' / 'nonstationary'

function [d,x,v2]=get_corrupted_signal(TYPE, LENGTH)
    function V=get_AR1_noise(LENGTH,C,G)
        V     =zeros(LENGTH,1)                      ;
        for i=1:LENGTH
            if i==1
                V(i)=G(i)                           ;
            else
                V(i)=G(i)+C*V(i-1)                  ;
            end
        end
    end
n        =0:LENGTH-1                                ;
if strcmp    (TYPE,'stationary'   )
    g    =randn(LENGTH,1)                           ;
else
    if strcmp(TYPE,'nonstationary')
        g=randn(LENGTH,1).*linspace(0.5,2.5,LENGTH)';
    end
end
d        =sin(((0.05)*pi).*n+0)'                    ;
v1       =get_AR1_noise(LENGTH, 0.8,g)              ;
v2       =get_AR1_noise(LENGTH,-0.6,g)              ;
x        =d+v1                                      ;
end

