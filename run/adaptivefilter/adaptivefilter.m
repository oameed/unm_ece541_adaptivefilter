%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                            %%%
%%% ECE 541 PROBABILITY THEORY AND STOCHASTIC PROCESSES %%%
%%% FINAL PROJECT FALL 2020                             %%%
%%% ADAPTIVE FILTERS APPLIED TO NOISE CANCELLING        %%%
%%% by: OAMEED NOAKOASTEEN                              %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all
clear
clc
addpath(fullfile(pwd,'..','..','lib'))

length          =800                                            ;
beta            =0.25                                           ;
lambda          =0.95                                           ;


%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% STATIONARY SIGNALS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%
[d,x,v2]        =get_corrupted_signal('stationary'   ,length   );

y               =algWienerFilter     ( x,v2          ,       12);
plotter(d,x,v2,y,length,'WienerFilter')

y               =algNLMS             ( x,v2          ,beta  ,12);
plotter(d,x,v2,y,length,'stationaryNLMS')

y               =algRLS              ( x,v2          ,lambda,12);
plotter(d,x,v2,y,length,'stationaryRLS')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% NON-STATIONARY SIGNALS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[d,x,v2]        =get_corrupted_signal('nonstationary',length)   ;

y               =algNLMS             ( x,v2          ,beta  ,12);
plotter(d,x,v2,y,length,'nonstationaryNLMS')

y               =algRLS              ( x,v2          ,lambda,12);
plotter(d,x,v2,y,length,'nonstationaryRLS')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% RECOVERY OF CORRUPTED AUDIO %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[d,Fs,info,x,v2]=get_corrupted_signal_audio('sorry_dave')       ;

plotter_audio(d, Fs, info, 'sorry_dave'                 )
plotter_audio(x, Fs, info, 'sorry_dave_corrupted'       )
audiowrite   (strcat('sorry_dave_corrupted','.wav'),x,Fs)

y               =algNLMS             ( x,v2          ,beta  ,12);

plotter_audio(y, Fs, info, 'sorry_dave_recovered'       )
audiowrite   (strcat('sorry_dave_recovered','.wav'),y,Fs)

