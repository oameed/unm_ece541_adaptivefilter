#! /bin/bash

cd run/adaptivefilter

echo   ' RUNNING ADAPTIVE FILTER EXPERIMENTS '
matlab -nodisplay -nosplash -nodesktop -r "run('adaptivefilter.m');exit;"


